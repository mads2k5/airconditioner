# Ar condicionado

- A aplicação verifica a temperatura a cada 30 segundos utilizando a OpenWeatherApi
- A programação liga/desliga do aparelho é controlada utilizado quartz-scheduler 
- O usuario pode controlar o aparelho utilizando a interface rest:

## exemplos:

  curl --request GET --url 'http://localhost:8084/remote/status'

  curl --request PUT --url 'http://localhost:8084/remote/temperature?temperature=18.0'

  curl --request PUT --url 'http://localhost:8084/remote/wake-up?hour=01&minute=42'

  curl --request PUT --url 'http://localhost:8084/remote/sleep?hour=01&minute=41'

  curl --request PUT --url 'http://localhost:8084/remote/switch-power'

## Mqtt 
A comunicação com o ar condicionado é feita com o Eclipse Paho Mqttv3. As mensagens para o dispositivo são publicadas no tópicos "NTTAirConditionerHardwareTopic" e o aviso de que o ar despertou é feita na "NTTAirConditionerUserTopic" e podem ser acompanhadas em http://www.hivemq.com/demos/websocket-client/

![diagram](https://gitlab.com/mads2k5/airconditioner/-/raw/main/diagram.png)
