package com.challenge.dtt.airconditioner.remote;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalTime;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RemoteController.class)
class RemoteControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    RemoteService remoteService;

    @MockBean
    Scheduler scheduler;

    @Test
    public void testGetStatus() throws Exception {
        given(remoteService.getStatus())
                .willReturn(new AirConditionerStatusDTO(
                        false,
                        true,
                        21.0,
                        18.0,
                        LocalTime.MIN,
                        LocalTime.NOON,
                        false));
        mockMvc.perform(MockMvcRequestBuilders.get("/remote/status"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("powerOnState").value(false))
                .andExpect(jsonPath("autoTemperature").value(true))
                .andExpect(jsonPath("acTemperature").value(21.0))
                .andExpect(jsonPath("weatherTemperature").value(18.0))
                .andExpect(jsonPath("wakeUpTime").value("00:00:00"))
                .andExpect(jsonPath("sleepTime").value("12:00:00"))
                .andExpect(jsonPath("timerState").value(false));
    }

    @Test
    public void testSetTemperature() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/remote/temperature")
                        .param("temperature", "15.0"))
                .andExpect(status().isOk());
    }

}