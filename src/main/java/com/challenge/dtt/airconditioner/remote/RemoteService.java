package com.challenge.dtt.airconditioner.remote;

import com.challenge.dtt.airconditioner.AirConditioner;
import com.challenge.dtt.airconditioner.mqtt.MqttPublisher;
import com.challenge.dtt.airconditioner.scheduler.SchedulerService;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
public class RemoteService {

    @Autowired
    private AirConditioner airConditioner;

    @Autowired
    private MqttPublisher mqttPublisher;

    @Autowired
    private SchedulerService schedulerService;

    public void setTemperature(Double temperature) {
        airConditioner.setAcTemperature(temperature);
        mqttPublisher.notifyHardwareSetTemperature(temperature);
    }

    public AirConditionerStatusDTO getStatus() {
        return airConditioner.status();
    }

    public void switchPower() {
        airConditioner.setPowerOnState(!airConditioner.getPowerOnState());
        mqttPublisher.notifyHardwareSetPower(airConditioner.getPowerOnState());
    }

    public void switchPower(Boolean status) {
        airConditioner.setPowerOnState(status);
        mqttPublisher.notifyHardwareSetPower(status);
    }

    public void wakeUp() {
        airConditioner.setPowerOnState(Boolean.TRUE);
        mqttPublisher.notifyHardwareSetPower(Boolean.TRUE);
    }

    public void sleep() {
        airConditioner.setPowerOnState(Boolean.FALSE);
        mqttPublisher.notifyHardwareSetPower(Boolean.FALSE);
    }

    public void setWakeUp(Integer hour, Integer minute) throws SchedulerException {
        LocalTime wakeUpTime = LocalTime.of(hour, minute);
        airConditioner.setTimerState(Boolean.TRUE);
        airConditioner.setWakeUpTime(wakeUpTime);
        schedulerService.setWakeUp(wakeUpTime);
    }

    public void setSleep(Integer hour, Integer minute) throws SchedulerException {
        LocalTime sleepTime = LocalTime.of(hour, minute);
        airConditioner.setTimerState(Boolean.TRUE);
        airConditioner.setSleepTime(sleepTime);
        schedulerService.setSleep(sleepTime);
    }
}
