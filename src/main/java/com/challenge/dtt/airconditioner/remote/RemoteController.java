package com.challenge.dtt.airconditioner.remote;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("remote")
public class RemoteController {

    @Autowired
    private RemoteService remoteService;

    @Autowired
    private Scheduler scheduler;

    @GetMapping("/status")
    public AirConditionerStatusDTO status() {
        return remoteService.getStatus();
    }

    @PutMapping("/temperature")
    public void temperature(@RequestParam Double temperature) {
        remoteService.setTemperature(temperature);
    }

    @PutMapping("/wake-up")
    public void wakeUp(@RequestParam Integer hour,@RequestParam Integer minute) throws SchedulerException {
        remoteService.setWakeUp(hour, minute);
    }

    @PutMapping("/sleep")
    public void sleep(@RequestParam Integer hour,@RequestParam Integer minute) throws SchedulerException {
        remoteService.setSleep(hour, minute);
    }

    @PutMapping("/switch-power")
    public void switchStatus() {
        remoteService.switchPower();
    }

}
