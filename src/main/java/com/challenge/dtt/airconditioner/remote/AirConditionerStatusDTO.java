package com.challenge.dtt.airconditioner.remote;

import java.time.LocalTime;

public class AirConditionerStatusDTO {

    private Boolean powerOnState;
    private Boolean autoTemperature;
    private Double weatherTemperature;
    private Double acTemperature;
    private LocalTime wakeUpTime;
    private LocalTime sleepTime;
    private Boolean timerState;


    public AirConditionerStatusDTO(Boolean powerOnState, Boolean autoTemperature, Double acTemperature, Double weatherTemperature, LocalTime wakeUpTime, LocalTime sleepTime, Boolean timerState) {
        this.powerOnState = powerOnState;
        this.autoTemperature = autoTemperature;
        this.acTemperature = acTemperature;
        this.weatherTemperature = weatherTemperature;
        this.wakeUpTime = wakeUpTime;
        this.sleepTime = sleepTime;
        this.timerState = timerState;
    }

    public Boolean getPowerOnState() {
        return powerOnState;
    }

    public void setPowerOnState(Boolean powerOnState) {
        this.powerOnState = powerOnState;
    }

    public Boolean getAutoTemperature() {
        return autoTemperature;
    }

    public void setAutoTemperature(Boolean autoTemperature) {
        this.autoTemperature = autoTemperature;
    }

    public Double getAcTemperature() {
        return acTemperature;
    }

    public void setAcTemperature(Double acTemperature) {
        this.acTemperature = acTemperature;
    }

    public LocalTime getWakeUpTime() {
        return wakeUpTime;
    }

    public void setWakeUpTime(LocalTime wakeUpTime) {
        this.wakeUpTime = wakeUpTime;
    }

    public LocalTime getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(LocalTime sleepTime) {
        this.sleepTime = sleepTime;
    }

    public Boolean getTimerState() {
        return timerState;
    }

    public void setTimerState(Boolean timerState) {
        this.timerState = timerState;
    }

    public Double getWeatherTemperature() {
        return weatherTemperature;
    }

    public void setWeatherTemperature(Double weatherTemperature) {
        this.weatherTemperature = weatherTemperature;
    }
}
