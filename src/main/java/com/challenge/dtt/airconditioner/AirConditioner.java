package com.challenge.dtt.airconditioner;

import com.challenge.dtt.airconditioner.remote.AirConditionerStatusDTO;
import com.challenge.dtt.airconditioner.weather.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Objects;

@Service
@Scope("singleton")
public class AirConditioner {

    private static final Logger logger = LoggerFactory.getLogger(AirConditioner.class);

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private Boolean powerOnState = Boolean.FALSE;
    private Boolean autoTemperature = Boolean.TRUE;
    private Double weatherTemperature = 20.0;
    private Double acTemperature = 20.0;
    private LocalTime wakeUpTime = LocalTime.MIN;
    private LocalTime sleepTime = LocalTime.NOON;
    private Boolean timerState = Boolean.FALSE;


    public Double getAcTemperature() {
        return acTemperature;
    }

    public void setAcTemperature(Double acTemperature) {
        this.acTemperature = acTemperature;
    }

    public Boolean getPowerOnState() {
        return powerOnState;
    }

    public void setPowerOnState(Boolean powerOnState) {
        this.powerOnState = powerOnState;
    }

    public Boolean getAutoTemperature() {
        return autoTemperature;
    }

    public void setAutoTemperature(Boolean autoTemperature) {
        this.autoTemperature = autoTemperature;
    }

    public Double getWeatherTemperature() {
        return weatherTemperature;
    }

    public void setWeatherTemperature(Double weatherTemperature) {
        this.weatherTemperature = weatherTemperature;
    }

    public LocalTime getWakeUpTime() {
        return wakeUpTime;
    }

    public void setWakeUpTime(LocalTime wakeUpTime) {
        this.wakeUpTime = wakeUpTime;
    }

    public LocalTime getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(LocalTime sleepTime) {
        this.sleepTime = sleepTime;
    }

    public Boolean getTimerState() {
        return timerState;
    }

    public void setTimerState(Boolean timerState) {
        this.timerState = timerState;
    }

    public AirConditionerStatusDTO status() {
        return new AirConditionerStatusDTO(
                powerOnState,
                autoTemperature,
                acTemperature,
                weatherTemperature,
                wakeUpTime,
                sleepTime,
                timerState
        );
    }

    public void setTemperatureManually(Double temperature) {
        this.autoTemperature = Boolean.FALSE;
        this.acTemperature = temperature;
    }

    @Scheduled(fixedRate = 30000)
    private void getWeather() {
        if(!powerOnState || !autoTemperature) {
            return;
        }
        Double temp = weatherService.getWeather();
        setWeatherTemperature(temp);

        if(!Objects.equals(temp, getAcTemperature())) {
            logger.info("weatherTemp != acTemp - Publishing event.");
            TemperaturePayload eventPayload = new TemperaturePayload(this, temp);
            applicationEventPublisher.publishEvent(eventPayload);
        }
    }
}
