package com.challenge.dtt.airconditioner.scheduler;

import com.challenge.dtt.airconditioner.remote.RemoteService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

public class SleepJob implements Job {

    @Autowired
    private RemoteService remoteService;

    @Override
    public void execute(JobExecutionContext context) {
        remoteService.sleep();
    }
}