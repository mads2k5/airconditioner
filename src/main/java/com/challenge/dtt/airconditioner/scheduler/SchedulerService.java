package com.challenge.dtt.airconditioner.scheduler;

import com.challenge.dtt.airconditioner.weather.WeatherService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;

@Service
public class SchedulerService {

    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    @Autowired
    private Scheduler scheduler;

    private JobDetail newJob(Class classJob, String identity, String description) {
        return JobBuilder.newJob().ofType(classJob).storeDurably()
                .withIdentity(JobKey.jobKey(identity))
                .withDescription(description)
                .build();
    }

    private Trigger trigger(JobDetail jobDetail, LocalTime wakeUpTime) {
        return newTrigger()
                .withIdentity(jobDetail.getKey().getName(), jobDetail.getKey().getGroup())
                .withSchedule(dailyAtHourAndMinute(wakeUpTime.getHour(), wakeUpTime.getMinute()))
                .build();
    }

    public void setWakeUp(LocalTime wakeUpTime) throws SchedulerException {
        deleteJob("DEFAULT", "WakeUpJob");
        JobDetail wakeUpJob = newJob(WakeUpJob.class, "WakeUpJob", "WakeUp at " + wakeUpTime);
        Trigger trigger = trigger(wakeUpJob, wakeUpTime);
        scheduler.scheduleJob(wakeUpJob, trigger);
    }

    public void setSleep(LocalTime sleepTime) throws SchedulerException {
        deleteJob("DEFAULT", "SleepJob");
        JobDetail sleepJob = newJob(SleepJob.class, "SleepJob", "Sleep at " + sleepTime);
        Trigger trigger = trigger(sleepJob, sleepTime);
        scheduler.scheduleJob(sleepJob, trigger);
    }

    public void deleteJob(String group, String name) {
        try {
            scheduler.deleteJob(jobKey(name, group));
            logger.info("Deleted job with key - " + group + "." +name);
        } catch (SchedulerException e) {
            logger.info("Could not delete job with key - " + group + "." +name + " due to error - " + e.getLocalizedMessage());
        }
    }

}
