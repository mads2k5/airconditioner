package com.challenge.dtt.airconditioner.events;

import com.challenge.dtt.airconditioner.AirConditioner;
import com.challenge.dtt.airconditioner.mqtt.MqttPublisher;
import com.challenge.dtt.airconditioner.TemperaturePayload;
import com.challenge.dtt.airconditioner.remote.RemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class TemperatureEvent implements ApplicationListener<TemperaturePayload> {

    @Autowired
    private AirConditioner airConditioner;

    @Autowired
    private RemoteService remoteService;

    @Autowired
    private MqttPublisher mqttPublisher;

    @Override
    public void onApplicationEvent(TemperaturePayload event) {
        Double WeatherTemperature = event.getTemperature();

        if (airConditioner.getAutoTemperature()) {
            if (WeatherTemperature < 15.0) {
                remoteService.switchPower(Boolean.FALSE);
            } else if( WeatherTemperature > 22.0) {
                remoteService.switchPower(Boolean.TRUE);
                mqttPublisher.notifyUserAcWakeUp();
            }
        }
    }
}
