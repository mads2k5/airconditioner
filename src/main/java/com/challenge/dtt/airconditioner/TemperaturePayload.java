package com.challenge.dtt.airconditioner;

import org.springframework.context.ApplicationEvent;

public class TemperaturePayload extends ApplicationEvent {

    private Double temperature;

    public TemperaturePayload(Object source, Double temperature) {
        super(source);
        this.temperature = temperature;
    }

    public Double getTemperature() {
        return temperature;
    }

}
