package com.challenge.dtt.airconditioner.weather;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public final class WeatherService {

    private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${openweathermap.base-url}")
    private String baseUrl;
    @Value("${openweathermap.path}")
    private String path;
    @Value("${openweathermap.api-key}")
    private String apiKey;


    public Double getWeather() {
        String city = "Curitiba";
        String url = String.format("https://%s%s?q=%s&units=metric&appid=%s", baseUrl, path, city, apiKey);

        ResponseEntity<WeatherResponse> forEntity = restTemplate.getForEntity(url, WeatherResponse.class);
        WeatherResponse body = forEntity.getBody();
        Double temp = body.getTemperature().getTemp();

        logger.info("Temperature: " + temp);
        return temp;
    }

}
