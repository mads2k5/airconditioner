package com.challenge.dtt.airconditioner.weather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

    @JsonProperty("main")
    private Temperature temperature;

    public Temperature getTemperature() {
        return temperature;
    }

    public class Temperature {
        public Double getTemp() {
            return temp;
        }
        private Double temp;
    }

}

