package com.challenge.dtt.airconditioner.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

@Service
public class MqttPublisher implements Callable<Void> {

    @Autowired
    private IMqttClient client;

    @Value("${mqtt.topic.user}")
    private String userTopic;
    @Value("${mqtt.topic.hardware}")
    private String hardwareTopic;

    @Override
    public Void call() throws Exception {
        sendMessage(null, null);
        return null;
    }

    private void sendMessage(String topic, Map<String, String> payload){
        if ( !client.isConnected()) {
            return;
        }
        MqttMessage msg = buildMessage(payload);
        msg.setQos(0);
        msg.setRetained(true);

        try {
            client.publish(topic, msg);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private MqttMessage buildMessage(Map<String, String> payload) {
        byte[] toSend = payload.toString()
                .getBytes();
        return new MqttMessage(toSend);
    }

    public void notifyHardwareSetPower(Boolean state) {
        Map<String, String> msg = new HashMap<>();
        msg.put("command", "set-power");
        msg.put("value", state.toString());
        msg.put("timestamp", String.valueOf(Instant.now().toEpochMilli()));
        sendMessage(hardwareTopic, msg);
    }

    public void notifyHardwareSetTemperature(Double temp) {
        Map<String, String> msg = new HashMap<>();
        msg.put("command", "set-temperature");
        msg.put("value", temp.toString());
        msg.put("timestamp", String.valueOf(Instant.now().toEpochMilli()));
        sendMessage(hardwareTopic, msg);
    }

    public void notifyUserAcWakeUp() {
        Map<String, String> msg = new HashMap<>();
        msg.put("message", "Ac wake up");
        msg.put("timestamp", String.valueOf(Instant.now().toEpochMilli()));
        sendMessage(userTopic, msg);
    }

}
