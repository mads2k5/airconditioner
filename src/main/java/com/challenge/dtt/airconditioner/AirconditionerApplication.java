package com.challenge.dtt.airconditioner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AirconditionerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirconditionerApplication.class, args);
	}

}
